# makefile for lsyslog library for Lua

# change these to reflect your Lua installation
LUAINC= /usr/include
LUALIB= /usr/lib
LUABIN= /usr/bin

# no need to change anything below here
CFLAGS= $(INCS) $(WARN) -O2 -fPIC
WARN= -pedantic -Wall
INCS= -I$(LUAINC)

MYNAME= syslog
MYLIB= $(MYNAME)
T= $(MYLIB).so
OBJS= l$(MYLIB).o
TEST= test.lua

all:	test

test:	$T
	LUA_CPATH='./?.so' $(LUABIN)/lua $(TEST)

o:	$(MYLIB).o

so:	$T

$T:	$(OBJS)
	$(CC) -o $@ -shared $(OBJS)

clean:
	rm -f $(OBJS) $T core core.* a.out
