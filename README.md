lsyslog: a Lua wrapper for the syslog C API
-------------------------------------------

### What is it

lsyslog is a Lua module that wraps the 
[syslog(3)](http://www.die.net/doc/linux/man/man3/syslog.3.html) C API.
It has been tested with Lua 5.2 on Linux.

### Download

The latest version can be found at [gitorious](https://gitorious.org/lsyslog).

### Compiling

To compile the module, just edit the `Makefile` to reflect your installation of
Lua. Then run `make`. This will build the library and run a simple test.

### Installing

To intall the module, just copy `syslog.so` somewhere in your
[`LUA_CPATH`](http://www.lua.org/manual/5.2/manual.html#pdf-package.cpath).

### Usage

The library exports the following Lua functions:

* openlog(ident, option[, facility])
* syslog(priority, message)
* closelog()

Usage looks as follows:

	require "syslog"
	
	syslog.openlog("lua syslog", 0)
	syslog.syslog("LOG_WARNING", "Hi all " .. os.time())
	syslog.closelog()

You may want to wrap the standard `assert` and `error` Lua functions so that
they log the assertion or error before aborting the program. This can be useful
for post-mortem analysis of embedded or unattended Lua applications. You can
append the following code at the end of `strict.lua`, a module that can be
found in the `etc` directory of the Lua sources, and which is worth including
anyway.

	-- set the ident string to the lua filename that first included this module
	--local func_info = debug.getinfo(3, "S")
	--if func_info then
	--	syslog.openlog(func_info.short_src, syslog.LOG_ODELAY, "LOG_USER")
	--end
	
	-- make error() call syslog.syslog() too
	local _error = error
	_G.error = function(txt, level)
		if level then
			level = level + 1
		else
			level = 2
		end
		local info = debug.getinfo(level, "Sl")
		local msg
		if info.what == "C" then
			msg = "[C function] "
		else
			msg = string.format("[%s]:%d ", info.short_src, info.currentline)
		end
		syslog.syslog("LOG_ERR", msg .. (txt or ""))
		_error(txt, level)
	end
	
	-- make assert() call our modified error function
	_G.assert = function (...)
		local cond, txt = ...
		if not cond then
			error(txt or "assertion failed!", 2)
		end
		return ...
	end

Then call `require "strict"` at the top of your code.

### License

This code is hereby placed in the public domain.

### Thanks

Thanks to [Luiz Henrique de Figueiredo](http://www.tecgraf.puc-rio.br/~lhf) for
the Makefile.

